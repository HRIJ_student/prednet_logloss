import code
import sys

import numpy
import chainer
import chainer.functions as F
import chainer.links as L
#from convlstm import ConvLSTM
from data_utils import *
from discmixlog import DiscretizedMixtureLogistic as DiscMixLog

class PredNet(chainer.Chain):
    def __init__(self, channels, img_dims):
        self.ksize = 3
        self.ch = channels # [3, 4, 8, 16, 32, 100]
        self.N  = len(channels) - 1
        super(PredNet, self).__init__(
            DiscretizedMixLogistic = DiscMixLog(img_dims, self.ksize,
                                                L_in_channels=self.ch[-2],
                                                L_out_channels=self.ch[-1]
                                                ),
            )

        # create conv layers
        for i in xrange(self.N-1):
            self.add_link('Conv_' + str(i), 
                L.Convolution2D(self.ch[i], self.ch[i+1], self.ksize, pad=1))

    def __call__(self, x, x_true=None, epoch=None, t=None):
        batchsize, n_channels, height, width = x.data.shape

        # pass through initial conv layers
        h = x 
        for i in xrange(self.N-1):
            conv = getattr(self, 'Conv_' + str(i))
            h = F.relu(conv(h))
        a0 = x
        log_loss, a0_hat = self.DiscretizedMixLogistic(a0, h)

        if x_true is not None:
            a0 = a0_hat

        self.L2 = F.mean_squared_error(a0, a0_hat)

        if x_true is not None:
            errors = F.mean_squared_error(x_true, a0_hat)
        else:
            errors  = self.L2

        #code.interact(local=dict(globals(), **locals()))
        #sys.exit()
        return log_loss / batchsize, errors, a0_hat
               



