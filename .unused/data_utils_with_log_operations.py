import numpy as np
import os
import chainer
import chainer.functions as F
from chainer import cuda
import code
import sys

from path_settings import *


def load_data():
    source_train = np.load(DATA_DIR + 'source_train.npy')
    source_val   = np.load(DATA_DIR + 'source_val.npy')
    data_train   = np.load(DATA_DIR + 'x_train.npy')
    data_val     = np.load(DATA_DIR + 'x_val.npy')
    return data_train, data_val, source_train, source_val


def load_test_data():
    source_test = np.load(DATA_DIR + 'source_test.npy')
    data_test   = np.load(DATA_DIR + 'x_test_orig.npy')
    return data_test, source_test


def get_possible_start_times(sources, nt):
    possible_starts = np.array([i for i in range(sources.shape[0] - nt) if sources[i] == sources[i + nt - 1]])
    return possible_starts
    

def choose_start_times(possible_starts, nt):
    n = possible_starts.shape[0]
    n_samples = n//10
    tmp1 = np.sort(np.random.choice(possible_starts, n_samples*2, replace=False))
    tmp2 = [tmp1[0]]
    t = tmp1[0]
    for i in xrange(1, n_samples*2, 1):
        if tmp1[i] - t > nt:
            tmp2.extend([tmp1[i]])
            t = tmp1[i]
        if len(tmp2) >= n_samples:
            break
    return np.asarray(tmp2)


def create_batches(data, possible_starts, batch_size, nt, samples_per_epoch):
    n_frames, n_channels, h, w = data.shape
    n_batches = min(possible_starts.shape[0], samples_per_epoch)/batch_size
    x_all = np.zeros((n_batches, batch_size * nt, n_channels, h, w), dtype=np.float32)
    for i in xrange(n_batches*batch_size):
        start = possible_starts[i]
        j = i/batch_size
        k = i%batch_size
        x_all[j, k*nt:(k+1)*nt, :, :, :] = data[start:start+nt]
    return x_all/255.
    
    # When you visualize a frame in 'x_all', you need to convert its dtype from np.float32 to np.unint8! 

    

#############################################################################################################################################

# CORRECTED 
xp = cuda.cupy
'''
try:
    if cuda.get_device(0).use():
        xp = cuda.cupy
except:
    xp = np
'''
# ported from tensorflow implementation of https://github.com/openai/pixel-cnn

def log_prob_from_logits(x):
    """ numerically stable log_softmax implementation that prevents overflow """
    axis = len(x.data.shape)-1
    m = F.max(x, axis=axis, keepdims=True)
    m = F.broadcast_to(m, x.data.shape)
    tmp = F.log(F.sum(F.exp(x-m), axis=axis))
    tmp = F.expand_dims(tmp, -1)
    return x - m - F.broadcast_to(tmp, x.data.shape)

def log_sum_exp(x):
    """ numerically stable log_sum_exp implementation that prevents overflow """
    axis = len(x.data.shape)-1
    m  = F.max(x, axis=axis)
    m2 = F.max(x, axis, keepdims=True)
    m2 = F.broadcast_to(m2, x.data.shape)
    return m + F.log(F.sum(F.exp(x-m2), axis=axis))

#   discretized logistic mixture likelihood for RGB-image
#   

def discretized_mix_logistic_loss(x_,l_,sum_all=True): 
    # convert dims to TF-style, makes slicing/indexing simpler
    x = F.rollaxis(x_, 1, 4)
    l = F.rollaxis(l_, 1, 4)
    
    """ 
    log-likelihood for mixture of discretized logistics, 
    assumes the data has been rescaled to [-1,1] interval 
    """
    xs = x.shape       # true (i.e. labels) to regress to, e.g. (B,32,32,3)
    ls = l.shape       # predicted distribution, e.g. (B,32,32,100)
    
    # unpacking the params of the mixture of logistics
    nr_mix = int(ls[-1]/10) 
    logit_probs, l = F.split_axis(l, np.array([nr_mix]), axis=-1)
    l = F.reshape(l, xs + (nr_mix*3,))

    means, log_scales, coeffs = F.split_axis(l, 3, axis=-1)
    
    log_scales = F.clip(log_scales, -7., 1e10)
    coeffs     = F.tanh(coeffs)
    
    #getting the means and adjusting them based on preceding sub-pixels
    x  = F.broadcast_to(F.reshape(x, xs + (1,)), xs + (nr_mix,))                
    _x0, _x1, _x2 = F.split_axis(x, 3, axis=3)
    _m0, _m1, _m2 = F.split_axis(means, 3, axis=3)
    _c0, _c1, _c2 = F.split_axis(coeffs, 3, axis=3)
    
    m1 = F.reshape(_m1 + _c0 * _x0,             (xs[0],xs[1],xs[2],1,nr_mix))
    m2 = F.reshape(_m2 + _c1 * _x0 + _c2 * _x1, (xs[0],xs[1],xs[2],1,nr_mix))
    means = F.concat((_m0, m1, m2), axis=3)
    centered_x = x - means
    inv_stdv   = F.exp(-log_scales)
    plus_in    = inv_stdv * (centered_x + 1./255.)
    cdf_plus   = F.sigmoid(plus_in)
    min_in     = inv_stdv * (centered_x - 1./255.)
    cdf_min    = F.sigmoid(min_in)
    log_cdf_plus = plus_in - F.softplus(plus_in) # log prob for 0 edge case 
    log_one_minus_cdf_min = -F.softplus(min_in)  # log prob for 255 edge case
    cdf_delta   = cdf_plus - cdf_min              # prob for all other cases
    mid_in      = inv_stdv * centered_x    # MID IN WAS -inv_stdv * centered_x (MAY HAVE BEEN PROBLEM)
    log_pdf_mid = mid_in - log_scales - 2. * F.softplus(mid_in)

    log_probs = F.where(x.data< -0.999, log_cdf_plus, 
                    F.where(x.data > 0.999, log_one_minus_cdf_min, 
                        F.where(cdf_delta.data > 1e-5, 
                            F.log(F.clip(cdf_delta, 1e-12, 1e10)), 
                            log_pdf_mid - xp.log(127.5))))
    
    #code.interact(local=dict(globals(), **locals()))
    #sys.exit()
                      
    log_probs = F.sum(log_probs, axis=3) + log_prob_from_logits(logit_probs)
 
    if sum_all:
        return - F.sum(log_sum_exp(log_probs))
    else:
        return - F.sum(log_sum_exp(log_probs), axis=(1,2))
        
def sample_from_discretized_mix_logistic(l_, nr_mix=10, nf=3, num_coeffs=3):

    l  = F.rollaxis(l_, 1, 4)
    ls = l.shape # (4, 128, 160, 100)
    xs =  ls[:-1] + (nf,) # (4,128,160,3)
    num_channels = xs[-1]

    # logit prob
    logit_probs, l = F.split_axis(l, np.array([nr_mix]), axis=-1)
    rand_0 = xp.random.uniform(low=1e-5, 
                               high=1. - 1e-5, 
                               size=logit_probs.shape).astype(xp.float32)
    rand_0 = chainer.Variable(rand_0, volatile='auto')
    sel = F.argmax(logit_probs - F.log(-F.log(rand_0)), axis=-1)

    # (bs, h, w, 90)
    l = F.reshape(l, (xs + (3 * nr_mix,))) # (bs, h, w, 3, 10)
    means, log_scales, coeffs = F.split_axis(l, 3, axis=-1)
    log_scales = F.clip(log_scales, -7.,1e10)
    coeffs = F.tanh(coeffs)

    #code.interact(local=dict(globals(), **locals()))
    #sys.exit()
    means, log_scales, coeffs = select_mix(sel, means, log_scales,
                                           coeffs, xs, nr_mix)
    # sample from logistic & clip to interval
    # we don't actually round to the nearest 8bit value when sampling
    u = xp.random.uniform(low=1e-5, 
                          high=1. - 1e-5, 
                          size=means.shape).astype(xp.float32)
    u = chainer.Variable(u, volatile='auto')
    x = means + F.exp(log_scales) * (F.log(u) - F.log(1. - u))

    Y = sample_clip(x, coeffs, xs)
    Y = F.rollaxis(Y, -1, 1)

    return Y

def select_mix(sel, means, log_scales, coeffs, xs, nr_mix):
    flatdim = np.product(xs)

    sel0    = F.reshape(sel, xs[:-1] + (1,)) # (4,128,160,1)
    sel1    = F.broadcast_to(sel0, xs) # (4,128,160,3)
    flatsel = F.flatten(sel1) # (245760,)

    flatmeans = F.reshape(means, (flatdim, nr_mix))
    flatlogsc = F.reshape(log_scales, (flatdim, nr_mix))
    flatcoeff = F.reshape(coeffs, (flatdim, nr_mix))

    means      = F.reshape(F.select_item(flatmeans, flatsel), xs)
    log_scales = F.reshape(F.select_item(flatlogsc, flatsel), xs)
    coeffs     = F.reshape(F.select_item(flatcoeff, flatsel), xs)

    return means, log_scales, coeffs

def sample_clip(x, coeffs, xs):
    y0 = F.clip(x[:,:,:,0], -1., 1.)

    y1 = x[:,:,:,1] + (coeffs[:,:,:,0] * y0)
    y1 = F.clip(y1, -1., 1.)

    y2 = x[:,:,:,2] + (coeffs[:,:,:,1] * y0) + (coeffs[:,:,:,2] * y1)
    y2 = F.clip(y2, -1., 1.)

    y0 = F.reshape(y0, xs[:-1] + (1,))
    y1 = F.reshape(y1, xs[:-1] + (1,))
    y2 = F.reshape(y2, xs[:-1] + (1,))

    Y = F.concat((y0,y1,y2),axis=3)

    return Y


def sample_from_discretized_mix_logistic_prev(l_, nr_mix, nf, num_coeffs=3):
    l = xp.rollaxis(l_, 1, 4) 
    ls = l.shape
    xs = ls[:-1] + (nf,) 
    num_channels = xs[3] 

    # unpack parameters
    logit_probs, l = xp.split(l, np.array([nr_mix]), axis=3)
    rand_0 = xp.random.uniform(low=1e-5, high=1. - 1e-5, size=logit_probs.shape)
    sel = xp.argmax(logit_probs - xp.log(-xp.log(rand_0)), axis=3)

    # because computing one-hot vectors reduce computational efficacy of GPU 
    # where we can not use advanced indexing we put this burden to CPU 
    # where we can use advanced indexing. this hybrid operation increased 
    # GPU-util from ~10% to ~90%! and reduce 
    # comp. time ~37 sec/epoch from ~170 sec/epoch for CPU only 
    # or ~1200 sec/epoch for GPU only. without sampling 
    # comp. time is ~30 sec/epoch.    
    onehot_cpu = np.eye(nr_mix)[cuda.to_cpu(sel)]
    onehot = xp.reshape(cuda.to_gpu(onehot_cpu), xs[:-1] + (1, nr_mix,))


    coeffs, l = xp.split(l, np.array([nr_mix * num_coeffs]), axis=3)
    coeffs = xp.tanh(xp.reshape(coeffs, (xs[0], xs[1], xs[2], num_coeffs, nr_mix)))
    l      = xp.reshape(l, xs + (nr_mix * 2,))
    means, log_scales = xp.split(l, 2, axis=4)
    log_scales = xp.maximum(log_scales, -7.)


    means      = xp.sum(means * onehot, axis=4)
    log_scales = xp.sum(log_scales * onehot, axis=4)
    coeffs     = xp.sum(coeffs * onehot, axis=4)

    #code.interact(local=dict(globals(), **locals()))
    #sys.exit()
    
    # sample from logistic & clip to interval
    # we don't actually round to the nearest 8bit value when sampling
    u = xp.random.uniform(low=1e-5, high=1. - 1e-5, size=means.shape)
    x = means + xp.exp(log_scales)*(xp.log(u) - xp.log(1. - u))
    
    
    xns = xp.zeros(xs[:-1] + (num_channels,))
    xns[:,:,:,0] = xp.minimum(xp.maximum(x[:,:,:,0], -1.), 1.)
    k = 0

    y0 = x[:,:,:,0]
    y0 = xp.minimum(xp.maximum(y0,-1.),1.)
    y1 = x[:,:,:,1] + (coeffs[:,:,:,0] * x[:,:,:,0])
    y1 = xp.minimum(xp.maximum(y1,-1.),1.)
    y2 = x[:,:,:,2] + (coeffs[:,:,:,1] * x[:,:,:,0]) + (coeffs[:,:,:,2] * y1)
    y2 = xp.minimum(xp.maximum(y2,-1.),1.)

    y0 = xp.reshape(y0, xs[:-1] + (1,))
    y1 = xp.reshape(y1, xs[:-1] + (1,))
    y2 = xp.reshape(y2, xs[:-1] + (1,))

    Y = xp.concatenate((y0,y1,y2), axis=-1)

    Y = Y.astype(cuda.cupy.float32)
    Y = chainer.Variable(Y,volatile='auto')
    Y = F.rollaxis(Y,-1,1)

    return Y



############################################################################################################################################################################################

# ORIGINAL

xp = cuda.cupy
'''
try:
    if cuda.get_device(0).use():
        xp = cuda.cupy
except:
    xp = np
'''
# ported from tensorflow implementation of https://github.com/openai/pixel-cnn

def sigmoid(x):
    return 1./(1. + F.exp(-x))

def log_prob_from_logits(x):
    """ numerically stable log_softmax implementation that prevents overflow """
    axis = len(x.data.shape)-1
    m = F.max(x, axis=axis, keepdims=True)
    m = F.broadcast_to(m, x.data.shape)
    tmp = F.log(F.sum(F.exp(x-m), axis=axis))
    tmp = F.expand_dims(tmp, -1)
    return x - m - F.broadcast_to(tmp, x.data.shape)

def log_sum_exp(x):
    """ numerically stable log_sum_exp implementation that prevents overflow """
    axis = len(x.data.shape)-1
    m  = F.max(x, axis=axis)
    m2 = F.max(x, axis, keepdims=True)
    m2 = F.broadcast_to(m2, x.data.shape)
    return m + F.log(F.sum(F.exp(x-m2), axis=axis))

#   discretized logistic mixture likelihood for RGB-image
#   

def discretized_mix_logistic_loss(x_,l_,sum_all=True): 
    # For an image, tf usually uses data shapes like (batch-size, width, height, channels).
    # However, chainer usually use data shapes like  (batch-size, channels, width, height).
    # At first, we convert data shapes of inputs into tf-style! 
    x = F.rollaxis(x_, 1, 4)
    l = F.rollaxis(l_, 1, 4)
    
    """ 
    log-likelihood for mixture of discretized logistics, 
    assumes the data has been rescaled to [-1,1] interval 
    """
    xs = x.data.shape       # true image (i.e. labels) to regress to, e.g. (B,32,32,3)
    ls = l.data.shape       # predicted distribution, e.g. (B,32,32,100)
    
    # unpacking the params of the mixture of logistics
    nr_mix = int(ls[-1]/10) 
    logit_probs, l = F.split_axis(l, np.array([nr_mix]), axis=3)
    l = F.reshape(l, xs + (nr_mix*3,))
    means, log_scales, coeffs = F.split_axis(l, 3, axis=4)
    #code.interact(local=dict(globals(), **locals()))
    #sys.exit()
    log_scales = F.maximum(log_scales, chainer.Variable(xp.ones_like(log_scales.data, dtype=xp.float32)*-7., volatile='auto'))
    coeffs = F.tanh(coeffs)
    
    #getting the means and adjusting them based on preceding sub-pixels
    x  = F.broadcast_to(F.reshape(x, xs + (1,)), xs + (nr_mix,))                     
    _x0, _x1, _x2 = F.split_axis(x, 3, axis=3)
    _m0, _m1, _m2 = F.split_axis(means, 3, axis=3)
    _c0, _c1, _c2 = F.split_axis(coeffs, 3, axis=3)
    
    m1 = F.reshape(_m1 + _c0 * _x0,             (xs[0],xs[1],xs[2],1,nr_mix))
    m2 = F.reshape(_m2 + _c1 * _x0 + _c2 * _x1, (xs[0],xs[1],xs[2],1,nr_mix))
    means = F.concat((_m0, m1, m2), axis=3)
    centered_x = x - means
    inv_stdv = F.exp(-log_scales)
    plus_in = inv_stdv * (centered_x + 1./255.)
    cdf_plus = sigmoid(plus_in)
    min_in = inv_stdv * (centered_x - 1./255.)
    cdf_min = sigmoid(min_in)
    log_cdf_plus = plus_in - F.softplus(plus_in) # log prob for 0 edge case 
    log_one_minus_cdf_min = -F.softplus(min_in)  # log prob for 255 edge case
    cdf_delta = cdf_plus - cdf_min               # prob for all other cases
    mid_in = - inv_stdv * centered_x
    log_pdf_mid = mid_in - log_scales - 2.*F.softplus(mid_in)
    const1 = chainer.Variable(xp.ones_like(cdf_delta.data, 
                            dtype=xp.float32), volatile='auto')*1e-12
    const2 = chainer.Variable(xp.ones_like(cdf_delta.data, 
                            dtype=xp.float32), volatile='auto')*xp.log(127.5)
    
    log_probs = F.where(x.data< -0.999,        log_cdf_plus, 
                F.where(x.data > 0.999,        log_one_minus_cdf_min , 
                F.where(cdf_delta.data > 1e-5, F.log(F.maximum(cdf_delta, const1)), 
                                               log_pdf_mid - const2
                       )))
                      
    log_probs = F.sum(log_probs, axis=3) + log_prob_from_logits(logit_probs)
    
    if sum_all:
        return - F.sum(log_sum_exp(log_probs))
    else:
        return - F.sum(log_sum_exp(log_probs), axis=[1,2])



#   numpy function
#   l_  : np.ndarray
#        
#
def sample_from_discretized_mix_logistic(l_, nr_mix, nf, num_coeffs):
    l = np.rollaxis(l_, 1, 4)
    ls = l.shape
    xs = ls[:-1] + (nf,)
    
    num_channels = xs[3]
    #num_coeffs = int(comb(num_channels, 2))
    
    # unpack parameters
    logit_probs, l = np.split(l, np.array([nr_mix]), axis=3)
    
    sel = np.argmax(logit_probs 
                - np.log(-np.log(np.random.uniform(low=1e-5, 
                            high=1. - 1e-5, size=logit_probs.shape))), axis=3)
    onehot = np.zeros(xs[0:2] + (nr_mix,))
    for i in xrange(xs[0]):
        onehot[i, np.arange(xs[1]), sel[i,0:xs[1],0]]=1
    #onehot = np.eye(nr_mix)[sel]    
    onehot = np.reshape(onehot, xs[0:2] + (1, 1, nr_mix,))
    
    coeffs, l = np.split(l, np.array([nr_mix * num_coeffs]), axis=3)
    coeffs = np.tanh(np.reshape(coeffs, (xs[0], xs[1], xs[2], num_coeffs, nr_mix)))
    l = np.reshape(l, xs + (nr_mix * 2,))
    means, log_scales = np.split(l, 2, axis=4)
    log_scales = np.maximum(log_scales, -7.)
    
    means = np.sum(means * onehot, axis=4)
    log_scales = np.sum(log_scales * onehot, axis=4)
    coeffs = np.sum(coeffs * onehot, axis=4)
    
    # sample from logistic & clip to interval
    # we don't actually round to the nearest 8bit value when sampling
    u = np.random.uniform(low=1e-5, high=1. - 1e-5, size=means.shape)
    x = means + np.exp(log_scales)*(np.log(u) - np.log(1. - u))
    
    xns = np.zeros(xs[:-1] + (num_channels,))
    xns[:,:,:,0] = np.minimum(np.maximum(x[:,:,:,0], -1.), 1.)
    k = 0
    for i in xrange(1, num_channels, 1):
        for j in xrange(i):
            xns[:,:,:,i] = x[:,:,:,i] + coeffs[:,:,:,k] * xns[:,:,:,j]
            k +=1
        xns[:,:,:,i] = np.minimum(np.maximum(xns[:,:,:,i], -1.), 1.)
    
    return xns



def sample_from_discretized_mix_logistic_gpu(l_, nr_mix, nf, num_coeffs=3):
    #l = xp.rollaxis(l_, 1, 4) #PREV
    l = F.rollaxis(l_, 1, 4)
    ls = l.shape
    xs = ls[:-1] + (nf,)
    
    num_channels = xs[3]
    
    # unpack parameters
    logit_probs, l = xp.split(l, np.array([nr_mix]), axis=3)
    
    sel = xp.argmax(logit_probs 
                    - xp.log(-xp.log(xp.random.uniform(low=1e-5, 
                        high=1. - 1e-5, size=logit_probs.shape))), axis=3)
    
    # because computing one-hot vectors reduce computational efficacy of GPU where we can not use advanced indexing
    # we put this burden to CPU where we can use advanced indexing.
    # this hybrid operation increased GPU-util from ~10% to ~90%!
    # and reduce comp. time ~37 sec/epoch from ~170 sec/epoch for CPU only or ~1200 sec/epoch for GPU only.
    # without sampling comp. time is ~30 sec/epoch.    
    onehot_cpu = np.eye(nr_mix)[cuda.to_cpu(sel)]
    #code.interact(local=dict(globals(), **locals()))
    #sys.exit()
    #onehot = xp.reshape(cuda.to_gpu(onehot_cpu), xs[0:2] + (1, 1, nr_mix,)) #PREV
    onehot = xp.reshape(cuda.to_gpu(onehot_cpu), xs[:-1] + (1, nr_mix,))
    
    coeffs, l = xp.split(l, np.array([nr_mix * num_coeffs]), axis=3)
    coeffs = xp.tanh(xp.reshape(coeffs, (xs[0], xs[1], xs[2], num_coeffs, nr_mix)))
    l = xp.reshape(l, xs + (nr_mix * 2,))
    means, log_scales = xp.split(l, 2, axis=4)
    log_scales = xp.maximum(log_scales, -7.)
    
    means = xp.sum(means * onehot, axis=4)
    log_scales = xp.sum(log_scales * onehot, axis=4)
    coeffs = xp.sum(coeffs * onehot, axis=4)
    
    # sample from logistic & clip to interval
    # we don't actually round to the nearest 8bit value when sampling
    u = xp.random.uniform(low=1e-5, high=1. - 1e-5, size=means.shape)
    x = means + xp.exp(log_scales)*(xp.log(u) - xp.log(1. - u))
    
    y = xp.zeros(xs[:-1] + (num_channels,))
    y[:,:,:,0] = xp.minimum(xp.maximum(x[:,:,:,0], -1.), 1.)
    k = 0
    """ # PREV
    for i in xrange(1, num_channels, 1):
        xns[:,:,:,i] = x[:,:,:,i]
        for j in xrange(i):
            xns[:,:,:,i] += coeffs[:,:,:,k] * xns[:,:,:,j]
            k +=1
        xns[:,:,:,i] = xp.minimum(xp.maximum(xns[:,:,:,i], -1.), 1.)
    """
    y[:,:,:,0] = x[:,:,:,0]
    
    y[:,:,:,1] = x[:,:,:,1] + coeffs[:,:,:,0] * x[:,:,:,0]

    y[:,:,:,2] = x[:,:,:,2] + coeffs[:,:,:,1] * x[:,:,:,0]
                            + coeffs[:,:,:,2] * x[:,:,:,1]


    # EVAN ADDED [
    xns = xns.astype(cuda.cupy.float32)
    xns = chainer.Variable(xns,volatile='auto')
    xns = F.rollaxis(xns,3,1)
    # EVAN ADDED ]
    
    return xns
