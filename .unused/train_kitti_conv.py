#!/usr/bin/env python

from __future__ import print_function
import argparse
import numpy as np
import os
import math
import code
import sys
import cPickle as pickle
import time

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import cuda
from chainer import optimizers
from chainer import serializers

import prednet
import data_utils
from path_settings import *

rng_seed = 98765

# Parser flags
parser = argparse.ArgumentParser()
parser.add_argument('--batchsize',       '-b', default=4,   type=int, help='batch size')
parser.add_argument('--initmodel',       '-m', default='',            help='Initialize the model from given file')
parser.add_argument('--resume',          '-r', default='',            help='Resume the optimization from snapshot')
parser.add_argument('--model_dir',       '-a', default=0.0,           help='directory where you can find a file storing model information')
parser.add_argument('--gpu',             '-g', default=-1,  type=int, help='GPU ID (negative value indicates CPU)')
parser.add_argument('--rng_seed',        '-e', default=rng_seed, type=int, help='seed for random number generator')
args = parser.parse_args()

# select and seed tensor backend
xp = cuda.cupy if args.gpu >= 0 else np
print('rng_seed = ',rng_seed)
xp.random.seed(args.rng_seed)
np.random.seed(args.rng_seed)

# model input settings
batch_size = 24
height = 128
width = 160
n_channels = [3, 4, 8, 16, 32, 100]

n_epochs = 400
samples_per_epoch = 1000
N_seq_val = 100

img_dims = (batch_size, n_channels[0], height, width)


# setup model
model = prednet.PredNet(n_channels,img_dims)
if args.gpu >= 0:
    cuda.get_device(args.gpu).use()
    print('GPU DEVICE: ',args.gpu)
    model.to_gpu()

# setup optimizer
lr = 0.001
optimizer = optimizers.Adam(alpha=lr, beta1=0.9, beta2=0.999, eps=1e-08)
optimizer.setup(model)
#code.interact(local=dict(globals(), **locals()))
#sys.exit()
# Init/Resume
"""
if args.initmodel:
    print('Load model from', args.initmodel)
    os.chdir(cwd+'/' + args.model_dir + '/results')
    serializers.load_npz(args.initmodel, model)
    os.chdir(subdir)
if args.resume:
    print('Load optimizer state from', args.resume)
    os.chdir(cwd+'/' + args.model_dir + '/results')
    serializers.load_npz(args.resume, optimizer)
    os.chdir(subdir)
os.chdir(subdir)
"""

# load data
print('training data: load()')
data_train, data_val, source_train, source_val = data_utils.load_data()

# create training set
possible_starts_train = data_utils.get_possible_start_times(source_train, batch_size)
possible_starts_train = np.random.permutation(possible_starts_train)
x_train = data_utils.create_conv_batches(data_train, possible_starts_train, batch_size, samples_per_epoch)
print('x_train.shape:', x_train.shape)
n_batches_train = x_train.shape[0]


# create validation set
possible_starts_val = data_utils.get_possible_start_times(source_val, batch_size)
possible_starts_val = np.random.permutation(possible_starts_val)
x_val = data_utils.create_conv_batches(data_val, possible_starts_val, batch_size, N_seq_val)
print('x_val.shape:  ', x_val.shape)

print('-------------------------------------------------------------')

loss_history = np.zeros((n_epochs, 3), dtype=np.float32)
# training loop
for epoch in xrange(1, n_epochs + 1, 1):
    t0 = time.time()
    
    # create mini-batches for training    
    possible_starts_train = np.random.permutation(possible_starts_train)
    x_train = data_utils.create_conv_batches(data_train, possible_starts_train, 
                                             batch_size, samples_per_epoch)
    
    loss_train_data = 0
    loss_disc_log_train  = 0
    # training loop
    for i in xrange(x_train.shape[0]):
        x_now = chainer.Variable(xp.asarray(x_train[i]).astype(xp.float32), volatile='off')
        model.zerograds()
        accum_loss_train = 0
        disc_mix_log_loss, L2_error, y_pred = model(x_now)
        loss_train_data     += L2_error.data
        loss_disc_log_train += disc_mix_log_loss.data

        # backprop
        disc_mix_log_loss.backward()
        disc_mix_log_loss.unchain_backward()
        optimizer.update()


    # validation loop

    loss_val_data = 0
    for i in xrange(x_val.shape[0]):
        x_now = chainer.Variable(xp.asarray(x_val[i]).astype(xp.float32), volatile='off')        
        accum_loss_val = 0
        disc_mix_log_loss, L2_error, y_pred = model(x_now)
        loss_val_data += L2_error.data

    loss_history[epoch -1, 0] = cuda.to_cpu(loss_train_data)/x_train.shape[0]
    loss_history[epoch -1, 1] = cuda.to_cpu(loss_val_data)  /x_val.shape[0]
    loss_history[epoch -1, 2] = cuda.to_cpu(loss_disc_log_train) / x_train.shape[0]
    #print('epoch: {0:d}    log_loss: {3:>10.2f}    L2_train: {1:>9.6f}    L2_val: {2:>9.6f}    time: {4:>6.2f}'.format(epoch, [epoch -1, 0], loss_history[epoch -1, 1], loss_history[epoch-1,2], time.time()-t0))
    #print('epoch: {0:d}    log_loss: {3:>10.2f}    L2_train: {1:>9.6f}    time: {4:>6.2f}'.format(epoch, [epoch -1, 0], loss_history[epoch-1,2], time.time()-t0))
    if epoch==1:
        print('epoch     log_loss         L2           VAL        time')
    print('{0:4d}    {1:>10.2f}    {2:>9.6f}    {3:9.6f}    {4:6.2f}'.format(epoch, loss_history[epoch-1,2], loss_history[epoch-1, 0], loss_history[epoch-1,1], time.time()-t0))

if not os.path.exists(MODEL_DIR): os.makedirs(MODEL_DIR)

print('save loss history')
np.save(MODEL_DIR + 'loss_history.npy', loss_history)
print('save the model')
serializers.save_npz(MODEL_DIR + 'prednet.model', model)
print('save the optimizer')
serializers.save_npz(MODEL_DIR + 'prednet.state', optimizer) 

     
        




