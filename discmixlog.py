import code
import sys

import numpy
import chainer
import chainer.functions as F
import chainer.links as L
from data_utils import *
from convlstm import ConvLSTM


class DiscretizedMixtureLogistic(chainer.Chain):
    """
    Link wrapping the discretized mixture logistic loss and
     sampling functions
    """
    def __init__(self, img_dims, filter_size, L_in, L_out, nr_mix=10):
        self.xs = img_dims[0:1] + img_dims[2:] + img_dims[1:2] # (4, 128, 160, 3)
        self.ls = self.xs[:-1] + (L_out,) # (4, 128, 160, 100)
        self.nr_mix = nr_mix # 10

        super(DiscretizedMixtureLogistic, self).__init__(

            LConv = L.Convolution2D(L_in, L_out, filter_size, pad=1),
            

            )

        #LConvLSTM = ConvLSTM(L_in_channels, L_out_channels, img_dims[2],img_dims[3], filter_size),

        self.discretized_mix_log_loss = None


    def logistic_loss(self, x_, means, log_scales, coeffs, logit_probs, sum_all=True):
        xs = self.xs
        nr_mix = self.nr_mix
        """ 
        log-likelihood for mixture of discretized logistics, 
        assumes the data has been rescaled to [-1,1] interval 
        """
        # getting the means and adjusting them based on preceding sub-pixels
        x = F.broadcast_to(F.reshape(x_, xs + (1,)), xs + (nr_mix,))
        _x0, _x1, _x2 = F.split_axis(x, 3, axis=3)
        _m0, _m1, _m2 = F.split_axis(means, 3, axis=3)
        _c0, _c1, _c2 = F.split_axis(coeffs, 3, axis=3)
        m1 = F.reshape(_m1 + _c0 * _x0,             (xs[0],xs[1],xs[2],1,nr_mix))
        m2 = F.reshape(_m2 + _c1 * _x0 + _c2 * _x1, (xs[0],xs[1],xs[2],1,nr_mix))
        means = F.concat((_m0, m1, m2), axis=3)

        centered_x = x - means
        inv_stdv   = F.exp(-log_scales)
        plus_in    = inv_stdv * (centered_x + 1./255.)
        cdf_plus   = F.sigmoid(plus_in)
        min_in     = inv_stdv * (centered_x - 1./255.)
        cdf_min    = F.sigmoid(min_in)
        log_cdf_plus = plus_in - F.softplus(plus_in) # log prob 0 edge case 
        log_one_minus_cdf_min = -F.softplus(min_in)  # log prob 255 edge case
        cdf_delta   = cdf_plus - cdf_min             # prob all other cases
        mid_in      = inv_stdv * centered_x    
        log_pdf_mid = mid_in - log_scales - 2. * F.softplus(mid_in)

        log_probs = F.where(x.data< -0.999, log_cdf_plus, 
                        F.where(x.data > 0.999, log_one_minus_cdf_min, 
                            F.where(cdf_delta.data > 1e-5, 
                                F.log(F.clip(cdf_delta, 1e-12, 1e10)), 
                                log_pdf_mid - self.xp.log(127.5))))

        # log_prob from logits
        logit_prob_max = F.max(logit_probs, axis=-1, keepdims=True)
        logit_prob_max = F.broadcast_to(logit_prob_max, logit_probs.shape)
        logit_prob_sum = F.logsumexp(logit_probs - logit_prob_max, axis=-1)
        logit_prob_sum = F.expand_dims(logit_prob_sum, -1)
        log_prob_from_logits =  logit_probs \
                              - logit_prob_max \
                              - F.broadcast_to(logit_prob_sum, logit_probs.shape)

        log_probs = F.sum(log_probs, axis=3) + log_prob_from_logits

        if sum_all:
            return -F.sum(F.logsumexp(log_probs, axis=-1))
        else:
            return -F.sum(F.logsumexp(log_probs, axis=-1), axis=(1,2))


    def sample_from_logistic(self, means, log_scales, coeffs, logit_probs):
        xs = self.xs
        nr_mix = self.nr_mix

        rand_0 = self.xp.random.uniform(low=1e-5, high=1. - 1e-5, 
                               size=logit_probs.shape).astype(self.xp.float32)
        rand_0 = chainer.Variable(rand_0, volatile='auto')
        sel = F.argmax(logit_probs - F.log(-F.log(rand_0)), axis=-1)

        # select mixture
        flatdim = np.product(xs)
        sel0    = F.reshape(sel, xs[:-1] + (1,)) # (4,128,160,1)
        sel1    = F.broadcast_to(sel0, xs) # (4,128,160,3)
        flatsel = F.flatten(sel1) # (245760,)

        flatmeans = F.reshape(means, (flatdim, nr_mix))
        flatlogsc = F.reshape(log_scales, (flatdim, nr_mix))
        flatcoeff = F.reshape(coeffs, (flatdim, nr_mix))

        means      = F.reshape(F.select_item(flatmeans, flatsel), xs)
        log_scales = F.reshape(F.select_item(flatlogsc, flatsel), xs)
        coeffs     = F.reshape(F.select_item(flatcoeff, flatsel), xs)

        # we don't actually round to the nearest 8bit value when sampling
        u = self.xp.random.uniform(low=1e-5, 
                                   high=1. - 1e-5, 
                                   size=means.shape).astype(self.xp.float32)
        u = chainer.Variable(u, volatile='auto')
        x = means + F.exp(log_scales) * (F.log(u) - F.log(1. - u))
        
        # sample from logistic and clip to interval
        y0 = F.clip(x[:,:,:,0], -1., 1.)
        y1 = x[:,:,:,1] + (coeffs[:,:,:,0] * y0)
        y1 = F.clip(y1, -1., 1.)
        y2 = x[:,:,:,2] + (coeffs[:,:,:,1] * y0) + (coeffs[:,:,:,2] * y1)
        y2 = F.clip(y2, -1., 1.)
    
        y0 = F.reshape(y0, self.xs[:-1] + (1,))
        y1 = F.reshape(y1, self.xs[:-1] + (1,))
        y2 = F.reshape(y2, self.xs[:-1] + (1,))
    
        Y = F.concat((y0,y1,y2),axis=3)
        # rescale and reshape for ret
        Y += 1
        Y /= 2
        Y = F.rollaxis(Y, -1, 1)

        return Y


    def __call__(self, x_, r):
        # rescale x to [-1,1] interval and reshape to tf dims
        #code.interact(local=dict(globals(), **locals()))
        #sys.exit()
        x = 2*x_ - 1
        x = F.rollaxis(x, 1, 4)
        l = F.rollaxis(self.LConv(r), 1, 4) # (bs, h, w, 100)

        # (bs, h, w, 10), (bs, h, w, 90)
        logit_probs, l = F.split_axis(l, np.array([self.nr_mix]), axis=-1)
        l = F.reshape(l, self.xs + (self.nr_mix*3,)) # (bs, h, w, 3, 30)
        means, log_scales, coeffs = F.split_axis(l, 3, axis=-1) #(bs,h,w,3,10)
        log_scales = F.clip(log_scales, -7., 1e10)
        coeffs     = F.tanh(coeffs)

        log_loss = self.logistic_loss(x,  means, log_scales, coeffs, logit_probs)
        x_hat = self.sample_from_logistic(means, log_scales, coeffs, logit_probs) 

        return log_loss, x_hat

