import numpy as np
import os
import chainer
import chainer.functions as F
from chainer import cuda
import code
import sys

from path_settings import *


def load_data():
    source_train = np.load(DATA_DIR + 'source_train.npy')
    source_val   = np.load(DATA_DIR + 'source_val.npy')
    data_train   = np.load(DATA_DIR + 'x_train.npy')
    data_val     = np.load(DATA_DIR + 'x_val.npy')
    return data_train, data_val, source_train, source_val


def load_test_data():
    source_test = np.load(DATA_DIR + 'source_test.npy')
    data_test   = np.load(DATA_DIR + 'x_test_orig.npy')
    return data_test, source_test


def get_possible_start_times(sources, nt):
    possible_starts = np.array([i for i in range(sources.shape[0] - nt) if sources[i] == sources[i + nt - 1]])
    return possible_starts
    

def choose_start_times(possible_starts, nt):
    n = possible_starts.shape[0]
    n_samples = n//10
    tmp1 = np.sort(np.random.choice(possible_starts, n_samples*2, replace=False))
    tmp2 = [tmp1[0]]
    t = tmp1[0]
    for i in xrange(1, n_samples*2, 1):
        if tmp1[i] - t > nt:
            tmp2.extend([tmp1[i]])
            t = tmp1[i]
        if len(tmp2) >= n_samples:
            break
    return np.asarray(tmp2)


def create_batches(data, possible_starts, batch_size, nt, samples_per_epoch):
    n_frames, n_channels, h, w = data.shape
    n_batches = min(possible_starts.shape[0], samples_per_epoch)/batch_size
    x_all = np.zeros((n_batches, batch_size * nt, n_channels, h, w), dtype=np.float32)
    for i in xrange(n_batches*batch_size):
        start = possible_starts[i]
        j = i/batch_size
        k = i%batch_size
        x_all[j, k*nt:(k+1)*nt, :, :, :] = data[start:start+nt]
    return x_all/255.
    
def create_conv_batches(data, possible_starts, batch_size, samples_per_epoch):
    n_frames, n_channels, h, w = data.shape
    n_batches = min(possible_starts.shape[0], samples_per_epoch)/batch_size
    x_all = np.zeros((n_batches, batch_size, n_channels, h, w), dtype=np.float32)
    for i in xrange(n_batches):
        for j in xrange(batch_size):
            start = possible_starts[i*batch_size]
            x_all[i,j,:,:,:] = data[start+j]
    return x_all/255.
    
    # When you visualize a frame in 'x_all', you need to convert its dtype from np.float32 to np.unint8! 

